# Lorem Picsum Browser #

A simple app for browsing images from [Lorem Picsum](https://picsum.photos/).

### Main functionality ###

* Loading paged data from Lorem Picsum
* Showing a spinner when the first page of data is loading
* Handling network errors and retries
* Using different column count for different screen widths
* Image details screen showing a full image and displaying author information

### Tools used ###

* Retrofit to fetch and parse JSON data from the network
* Glide to load images
* ViewModel and LiveData to retain data across configuration changes
* View Binding for null and type safe Views
* Jetpack Navigation for screen navigation
* Jetpack Paging for image list paging
* Koin for dependency injection
* Mockito for mocks in unit tests
* Espresso for instrumentation tests