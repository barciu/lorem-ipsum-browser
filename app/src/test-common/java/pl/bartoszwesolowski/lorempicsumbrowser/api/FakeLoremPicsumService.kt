package pl.bartoszwesolowski.lorempicsumbrowser.api

import retrofit2.Call
import retrofit2.mock.Calls
import kotlin.math.min

class FakeLoremPicsumService : LoremPicsumService {

    var imageList: List<ImageJson> = emptyList()
    var error: Throwable? = null
    var limit: Int? = null

    override fun getImages(page: Int, limit: Int): Call<List<ImageJson>> {
        return if (error != null) {
            Calls.failure(error!!)
        } else {
            val currentLimit = if (this.limit != null) min(this.limit!!, limit) else limit
            val fromIndex = min(imageList.size, (page - 1) * currentLimit)
            val toIndex = min(imageList.size, page * currentLimit)
            Calls.response(imageList.subList(fromIndex, toIndex))
        }
    }
}