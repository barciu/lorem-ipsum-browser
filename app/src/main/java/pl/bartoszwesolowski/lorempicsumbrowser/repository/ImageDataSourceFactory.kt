package pl.bartoszwesolowski.lorempicsumbrowser.repository

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import pl.bartoszwesolowski.lorempicsumbrowser.api.LoremPicsumService
import pl.bartoszwesolowski.lorempicsumbrowser.model.Image

class ImageDataSourceFactory(private val loremPicsumService: LoremPicsumService) : DataSource.Factory<Int, Image>() {
    val sourceLiveData = MutableLiveData<ImageDataSource>()
    override fun create(): DataSource<Int, Image> {
        val source = ImageDataSource(loremPicsumService)
        sourceLiveData.postValue(source)
        return source
    }
}
