package pl.bartoszwesolowski.lorempicsumbrowser.repository

import androidx.lifecycle.switchMap
import androidx.paging.toLiveData
import pl.bartoszwesolowski.lorempicsumbrowser.api.LoremPicsumService

class ImageRepository(private val loremPicsumService: LoremPicsumService) {

    fun getImageListing(pageSize: Int): ImageListing {
        val imageDataSourceFactory = ImageDataSourceFactory(loremPicsumService)
        return ImageListing(
            imageList = imageDataSourceFactory.toLiveData(pageSize = pageSize, initialLoadKey = 1),
            initialNetworkState = imageDataSourceFactory.sourceLiveData.switchMap { it.initialNetworkState },
            networkState = imageDataSourceFactory.sourceLiveData.switchMap { it.networkState },
            retry = { imageDataSourceFactory.sourceLiveData.value?.retry() }
        )
    }
}