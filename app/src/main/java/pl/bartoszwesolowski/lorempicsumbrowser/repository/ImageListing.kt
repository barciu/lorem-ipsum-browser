package pl.bartoszwesolowski.lorempicsumbrowser.repository

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import pl.bartoszwesolowski.lorempicsumbrowser.model.Image

data class ImageListing(
    val imageList: LiveData<PagedList<Image>>,
    val initialNetworkState: LiveData<NetworkState>,
    val networkState: LiveData<NetworkState>,
    val retry: () -> Unit
)