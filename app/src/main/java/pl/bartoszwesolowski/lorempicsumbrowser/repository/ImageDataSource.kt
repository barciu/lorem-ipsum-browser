package pl.bartoszwesolowski.lorempicsumbrowser.repository

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import pl.bartoszwesolowski.lorempicsumbrowser.api.ImageJson
import pl.bartoszwesolowski.lorempicsumbrowser.api.LoremPicsumService
import pl.bartoszwesolowski.lorempicsumbrowser.model.Image
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ImageDataSource(private val loremPicsumService: LoremPicsumService) : PageKeyedDataSource<Int, Image>() {

    private val totalCount = 1000

    private var retry: (() -> Any)? = null
    val initialNetworkState = MutableLiveData<NetworkState>()
    val networkState = MutableLiveData<NetworkState>()

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Image>) {
        initialNetworkState.postValue(NetworkState.LOADING)
        loremPicsumService.getImages(1, params.requestedLoadSize).enqueue(object : Callback<List<ImageJson>> {
            override fun onFailure(call: Call<List<ImageJson>>, t: Throwable) {
                retry = { loadInitial(params, callback) }
                initialNetworkState.postValue(NetworkState.FAILED)
            }

            override fun onResponse(call: Call<List<ImageJson>>, response: Response<List<ImageJson>>) {
                if (response.isSuccessful) {
                    retry = null
                    callback.onResult(response.body().toImageList(), null, 2)
                    initialNetworkState.postValue(NetworkState.SUCCESS)
                } else {
                    retry = { loadInitial(params, callback) }
                    initialNetworkState.postValue(NetworkState.FAILED)
                }
            }
        })
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Image>) {
        networkState.postValue(NetworkState.LOADING)
        loremPicsumService.getImages(params.key, params.requestedLoadSize).enqueue(object : Callback<List<ImageJson>> {
            override fun onFailure(call: Call<List<ImageJson>>, t: Throwable) {
                retry = { loadAfter(params, callback) }
                networkState.postValue(NetworkState.FAILED)
            }

            override fun onResponse(call: Call<List<ImageJson>>, response: Response<List<ImageJson>>) {
                if (response.isSuccessful) {
                    retry = null
                    val nextPageExists = params.key * params.requestedLoadSize < totalCount
                    val nextPage = if (nextPageExists) params.key + 1 else null
                    callback.onResult(response.body().toImageList(), nextPage)
                    networkState.postValue(NetworkState.SUCCESS)
                } else {
                    retry = { loadAfter(params, callback) }
                    networkState.postValue(NetworkState.FAILED)
                }
            }
        })
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Image>) {
        // ignore, since we always load next pages
    }

    fun retry() {
        retry?.invoke()
    }

    private fun List<ImageJson>?.toImageList(): List<Image> {
        return this?.map { imageJson -> Image(imageJson.id, imageJson.author, imageJson.download_url) } ?: emptyList()
    }
}