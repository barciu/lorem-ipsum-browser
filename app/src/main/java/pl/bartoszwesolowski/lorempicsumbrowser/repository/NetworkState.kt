package pl.bartoszwesolowski.lorempicsumbrowser.repository

enum class NetworkState {
    LOADING, SUCCESS, FAILED
}