package pl.bartoszwesolowski.lorempicsumbrowser.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Image(
    val id: String,
    val author: String,
    val url: String
) : Parcelable