package pl.bartoszwesolowski.lorempicsumbrowser.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import pl.bartoszwesolowski.lorempicsumbrowser.databinding.MainActivityBinding

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(MainActivityBinding.inflate(layoutInflater).root)
    }
}