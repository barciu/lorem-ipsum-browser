package pl.bartoszwesolowski.lorempicsumbrowser.ui

import androidx.lifecycle.ViewModel
import pl.bartoszwesolowski.lorempicsumbrowser.repository.ImageListing
import pl.bartoszwesolowski.lorempicsumbrowser.repository.ImageRepository

class ImageListViewModel(imageRepository: ImageRepository) : ViewModel() {

    private val imageListing: ImageListing = imageRepository.getImageListing(pageSize = 30)

    val imageList = imageListing.imageList
    val initialNetworkState = imageListing.initialNetworkState
    val networkState = imageListing.networkState

    fun retry() {
        imageListing.retry.invoke()
    }
}