package pl.bartoszwesolowski.lorempicsumbrowser.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import pl.bartoszwesolowski.lorempicsumbrowser.R
import pl.bartoszwesolowski.lorempicsumbrowser.databinding.ImageListItemBinding
import pl.bartoszwesolowski.lorempicsumbrowser.model.Image

class ImageListAdapter(private val onImageClickListener: OnImageClickListener) :
    PagedListAdapter<Image, ImageViewHolder>(ImageDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        return ImageViewHolder(ImageListItemBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        val image = getItem(position) ?: return
        holder.binding.image.setOnClickListener { onImageClickListener.onImageClick(image) }
        Glide.with(holder.binding.image)
            .load(image.url)
            .centerCrop()
            .placeholder(R.drawable.placeholder)
            .into(holder.binding.image)
    }

    override fun onViewRecycled(holder: ImageViewHolder) {
        Glide.with(holder.binding.image).clear(holder.binding.image)
    }
}

class ImageViewHolder(val binding: ImageListItemBinding) : RecyclerView.ViewHolder(binding.root)

private class ImageDiffCallback : DiffUtil.ItemCallback<Image>() {
    override fun areItemsTheSame(oldItem: Image, newItem: Image): Boolean = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Image, newItem: Image): Boolean = oldItem == newItem
}