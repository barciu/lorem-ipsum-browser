package pl.bartoszwesolowski.lorempicsumbrowser.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import pl.bartoszwesolowski.lorempicsumbrowser.R
import pl.bartoszwesolowski.lorempicsumbrowser.databinding.ImageDetailsBinding

class ImageDetailsFragment : Fragment() {

    private val args: ImageDetailsFragmentArgs by navArgs()

    private var _binding: ImageDetailsBinding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = ImageDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.author.text = getString(R.string.author, args.image.author)
        Glide.with(this)
            .load(args.image.url)
            .into(binding.image)
    }
}