package pl.bartoszwesolowski.lorempicsumbrowser.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import org.koin.android.viewmodel.ext.android.viewModel
import pl.bartoszwesolowski.lorempicsumbrowser.R
import pl.bartoszwesolowski.lorempicsumbrowser.databinding.ImageListBinding
import pl.bartoszwesolowski.lorempicsumbrowser.model.Image
import pl.bartoszwesolowski.lorempicsumbrowser.repository.NetworkState

class ImageListFragment : Fragment() {

    private val viewModel: ImageListViewModel by viewModel()

    private var _binding: ImageListBinding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = ImageListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val adapter = ImageListAdapter(object : OnImageClickListener {
            override fun onImageClick(image: Image) {
                findNavController().navigate(ImageListFragmentDirections.imageDetailsAction(image))
            }
        })
        binding.imageList.adapter = adapter
        viewModel.imageList.observe(viewLifecycleOwner, Observer { images ->
            adapter.submitList(images)
        })
        binding.retryButton.setOnClickListener { viewModel.retry() }
        viewModel.initialNetworkState.observe(viewLifecycleOwner, Observer { initialNetworkState ->
            when (initialNetworkState) {
                null                 -> binding.viewAnimator.displayedChild = CHILD_PROGRESS_BAR
                NetworkState.LOADING -> binding.viewAnimator.displayedChild = CHILD_PROGRESS_BAR
                NetworkState.SUCCESS -> binding.viewAnimator.displayedChild = CHILD_IMAGE_LIST
                NetworkState.FAILED  -> binding.viewAnimator.displayedChild = CHILD_RETRY_BUTTON
            }
        })
        viewModel.networkState.observe(viewLifecycleOwner, object : Observer<NetworkState?> {
            private var snackbar: Snackbar? = null
            override fun onChanged(networkState: NetworkState?) {
                if (networkState == NetworkState.FAILED) {
                    snackbar = Snackbar.make(view, R.string.fetching_images_failed, Snackbar.LENGTH_INDEFINITE)
                        .also {
                            it.setAction(R.string.retry_action) { viewModel.retry() }
                            it.show()
                        }
                } else {
                    snackbar?.dismiss()
                }
            }
        })
    }

    private companion object {
        const val CHILD_IMAGE_LIST = 0
        const val CHILD_PROGRESS_BAR = 1
        const val CHILD_RETRY_BUTTON = 2
    }
}