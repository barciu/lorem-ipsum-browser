package pl.bartoszwesolowski.lorempicsumbrowser.ui

import pl.bartoszwesolowski.lorempicsumbrowser.model.Image

interface OnImageClickListener {
    fun onImageClick(image: Image)
}