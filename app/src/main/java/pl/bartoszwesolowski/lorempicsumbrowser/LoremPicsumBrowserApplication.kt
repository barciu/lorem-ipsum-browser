package pl.bartoszwesolowski.lorempicsumbrowser

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import pl.bartoszwesolowski.lorempicsumbrowser.api.LoremPicsumService
import pl.bartoszwesolowski.lorempicsumbrowser.repository.ImageRepository
import pl.bartoszwesolowski.lorempicsumbrowser.ui.ImageListViewModel
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

@Suppress("unused")
class LoremPicsumBrowserApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@LoremPicsumBrowserApplication)
            modules(
                listOf(
                    module {
                        viewModel { ImageListViewModel(ImageRepository(get())) }
                        single<LoremPicsumService> {
                            Retrofit.Builder()
                                .baseUrl("https://picsum.photos/")
                                .addConverterFactory(MoshiConverterFactory.create())
                                .build()
                                .create(LoremPicsumService::class.java)
                        }
                    }
                )
            )
        }
    }
}