package pl.bartoszwesolowski.lorempicsumbrowser.api

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface LoremPicsumService {

    @GET("v2/list")
    fun getImages(
        @Query("page") page: Int,
        @Query("limit") limit: Int
    ): Call<List<ImageJson>>
}