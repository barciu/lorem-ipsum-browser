package pl.bartoszwesolowski.lorempicsumbrowser.api

data class ImageJson(
    val id: String,
    val author: String,
    val download_url: String
)