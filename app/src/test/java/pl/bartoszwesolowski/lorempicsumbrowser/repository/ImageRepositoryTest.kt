package pl.bartoszwesolowski.lorempicsumbrowser.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock
import pl.bartoszwesolowski.lorempicsumbrowser.api.FakeLoremPicsumService
import pl.bartoszwesolowski.lorempicsumbrowser.api.ImageJson
import pl.bartoszwesolowski.lorempicsumbrowser.model.Image
import java.io.IOException

class ImageRepositoryTest {

    @Suppress("unused")
    @get:Rule // used to make all live data calls sync
    val instantExecutor = InstantTaskExecutorRule()

    private val pageSize = 3

    private val loremPicsumService = FakeLoremPicsumService()
    private val repository = ImageRepository(loremPicsumService)

    /** asserts that empty list works fine */
    @Test
    fun emptyList() {
        val listing = repository.getImageListing(pageSize)
        val pagedList = getImageList(listing)
        assertThat(pagedList.size, `is`(0))
    }

    /** asserts that a list w/ single item is loaded properly */
    @Test
    fun oneItem() {
        loremPicsumService.imageList = createJsonImageList(1)
        val listing = repository.getImageListing(pageSize)
        val pagedList = getImageList(listing)
        assertThat(pagedList, `is`(createImageList(1)))
    }

    /** asserts loading a full list in multiple pages */
    @Test
    fun verifyCompleteList() {
        loremPicsumService.imageList = createJsonImageList(11)
        loremPicsumService.limit = pageSize
        val listing = repository.getImageListing(pageSize)
        // trigger loading of the whole list
        val pagedList = getImageList(listing)
        pagedList.loadAllData()
        assertThat(pagedList, `is`(createImageList(11)))
    }

    /** asserts the failure message when the initial load cannot complete */
    @Test
    fun failToLoadInitial() {
        loremPicsumService.error = IOException()
        val listing = repository.getImageListing(pageSize)
        getImageList(listing) // trigger load
        assertThat(getInitialNetworkState(listing), `is`(NetworkState.FAILED))
    }

    /** asserts the retry logic when initial load request fails */
    @Test
    fun retryInInitialLoad() {
        loremPicsumService.error = IOException()
        val listing = repository.getImageListing(pageSize)
        // trigger load
        val pagedList = getImageList(listing)
        assertThat(pagedList.size, `is`(0))

        @Suppress("UNCHECKED_CAST")
        val networkObserver = mock(Observer::class.java) as Observer<NetworkState>
        listing.initialNetworkState.observeForever(networkObserver)
        loremPicsumService.imageList = createJsonImageList(1)
        loremPicsumService.error = null
        listing.retry()
        assertThat(pagedList.size, `is`(1))
        assertThat(getInitialNetworkState(listing), `is`(NetworkState.SUCCESS))
        val inOrder = Mockito.inOrder(networkObserver)
        inOrder.verify(networkObserver).onChanged(NetworkState.FAILED)
        inOrder.verify(networkObserver).onChanged(NetworkState.LOADING)
        inOrder.verify(networkObserver).onChanged(NetworkState.SUCCESS)
        inOrder.verifyNoMoreInteractions()
    }

    /** asserts the retry logic when initial load succeeds but subsequent loads fails */
    @Test
    fun retryAfterInitialFails() {
        loremPicsumService.limit = pageSize
        loremPicsumService.imageList = createJsonImageList(11)
        val listing = repository.getImageListing(pageSize)
        val list = getImageList(listing)
        assertThat("test sanity, we should not load everything", list.size < 11, `is`(true))
        assertThat(getInitialNetworkState(listing), `is`(NetworkState.SUCCESS))
        loremPicsumService.error = IOException()
        list.loadAllData()
        assertThat(getNetworkState(listing), `is`(NetworkState.FAILED))
        loremPicsumService.error = null
        listing.retry()
        list.loadAllData()
        assertThat(getNetworkState(listing), `is`(NetworkState.SUCCESS))
        assertThat(list, `is`(createImageList(11)))
    }

    private fun createJsonImageList(size: Int) = (0 until size).map { createJsonImage(it) }

    private fun createImageList(size: Int) = (0 until size).map { createImage(it) }

    private fun createJsonImage(id: Int) = ImageJson("$id", "author$id", "url$id")

    private fun createImage(id: Int) = Image("$id", "author$id", "url$id")

    private fun getImageList(listing: ImageListing): PagedList<Image> {
        val observer = LoggingObserver<PagedList<Image>>()
        listing.imageList.observeForever(observer)
        assertThat(observer.value, `is`(notNullValue()))
        return observer.value!!
    }

    /** extract the initial network state from the listing */
    private fun getInitialNetworkState(listing: ImageListing): NetworkState? {
        val networkObserver = LoggingObserver<NetworkState>()
        listing.initialNetworkState.observeForever(networkObserver)
        return networkObserver.value
    }

    /** extract the network state from the listing */
    private fun getNetworkState(listing: ImageListing): NetworkState? {
        val networkObserver = LoggingObserver<NetworkState>()
        listing.networkState.observeForever(networkObserver)
        return networkObserver.value
    }

    /** simple observer that logs the latest value it receives */
    private class LoggingObserver<T> : Observer<T> {
        var value: T? = null
        override fun onChanged(t: T?) {
            this.value = t
        }
    }

    private fun <T> PagedList<T>.loadAllData() {
        do {
            val oldSize = this.loadedCount
            this.loadAround(this.size - 1)
        } while (this.size != oldSize)
    }
}